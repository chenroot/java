@ECHO OFF
REM The script sets environment variables helpful for PostgreSQL

@SET PATH="C:\PostgreSQL3\bin";%PATH%
@SET PGDATA=C:\PostgreSQL3\data
@SET PGDATABASE=postgres
@SET PGUSER=postgres
@SET PGPORT=5432
@SET PGLOCALEDIR=C:\PostgreSQL3\share\locale

                          